import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /**
    * Main method. Different tests.
    */
   public static void main(String[] param) {
      valueOf("3/5");
   }

   private long numerator;
   private long denominator;

   /**
    * Constructor.
    *
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction(long a, long b) {
      long gcd = greatestCommonDivisor(a, b);
      this.numerator = a / gcd;
      this.denominator = b / gcd;
      if (denominator == 0) {
         throw new RuntimeException("Denominator cannot be 0!");
      }
      if (denominator < 0) {
         numerator = -1 * numerator;
         denominator = -1 * denominator;
      }
      if (numerator == 0)  {
         denominator = 1;
      }
   }

   /**
    * Public method to access the numerator field.
    *
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /**
    * Public method to access the denominator field.
    *
    * @return denominator
    */
   public long getDenominator() {
      return denominator;
   }

   /**
    * Conversion to string.
    *
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   /**
    * Equality test.
    *
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals(Object m) {
      return compareTo((Lfraction) m) == 0;
   }

   /**
    * Hashcode has to be equal for equal fractions.
    *
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return (int) (37 * numerator + 13 * denominator);
   }

   /** Find the common denominator of given denominators
    *
    * @param denominator1 first denominator
    * @param denominator2 second denominator
    * @return common denominator
    */
   private long findCommonDenominator(long denominator1, long denominator2) {
      long newdenominator = 0;
      if (denominator1 == denominator2) {
         newdenominator = denominator1;
      } else { newdenominator = denominator1 * denominator2; }
      return newdenominator;
   }
   /**
    * Sum of fractions.
    *
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus(Lfraction m) {
      long newdenominator = findCommonDenominator(denominator, m.getDenominator());

      long numerator1 = (newdenominator / denominator) * numerator;
      long numerator2 = (newdenominator/ m.getDenominator()) * m.getNumerator();

      return new Lfraction(numerator1 + numerator2, newdenominator);
   }

   /**
    * Multiplication of fractions.
    *
    * @param m second factor
    * @return this*m
    */
   public Lfraction times(Lfraction m) {
      return new Lfraction(numerator * m.getNumerator(), denominator * m.getDenominator());
   }

   /**
    * Inverse of the fraction. n/d becomes d/n.
    *
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (denominator == 0) {
         throw new RuntimeException("Denominator can't be 0!");
      }
      return new Lfraction(denominator, numerator);
   }

   /**
    * Opposite of the fraction. n/d becomes -n/d.
    *
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-1 * numerator, denominator);
   }

   /**
    * Difference of fractions.
    * Done by adding an inverse fraction of the one given.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus(Lfraction m) {
      return plus(m.opposite());
   }

   /**
    * Quotient of fractions.
    *
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy(Lfraction m) {
      if (m.getNumerator() == 0) {
         throw new RuntimeException("Denominator of fraction cannot be 0! Got: " + m);
      }
      return times(m.inverse());
   }

   /**
    * Comparision of fractions.
    *
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo(Lfraction m) {
      long commonDenom = findCommonDenominator(denominator, m.getDenominator());
      long numerator1 = (commonDenom / denominator) * numerator;
      long numerator2 = (commonDenom / m.getDenominator()) * m.getNumerator();
      return Long.compare(numerator1, numerator2);
   }

   /**
    * Clone of the fraction.
    *
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);
   }

   /**
    * Integer part of the (improper) fraction.
    *
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator / denominator;
   }

   /**
    * Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    *
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long num = numerator % denominator;
      return new Lfraction(num, denominator);
   }

   /**
    * Approximate value of the fraction.
    *
    * @return numeric value of this fraction
    */
   public double toDouble() {
      double numdouble = Double.valueOf(numerator);
      double denodouble = Double.valueOf(denominator);
      return numdouble / denodouble;
   }

   /**
    * Double value f presented as a fraction with denominator d > 0.
    *
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction(double f, long d) {
      if (d == 0) {
         throw new RuntimeException("Denominator can't be 0! Got: " + d);
      }
      long num = Math.round(f * d);
      return new Lfraction(num, d);
   }

   /**
    * Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    *
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf(String s) {
      if (!(s.contains("/"))) {
         throw new RuntimeException("Fraction must be in format numerator/denominator! Got: " + s);
      }
      String[] a = s.split("/");
      if (!isNumeric(a[0])) {
         throw new RuntimeException("Numerator must be a number! " + a[0] + " in " + s);
      }
      if (!(isNumeric(a[1]))) {
         throw new RuntimeException("Denominator must be a number! " + a[1] + " in " + s);
      }
      long num = 0;
      long denom = 0;
      try {
         num = Long.parseLong(a[0]);
      } catch (NumberFormatException e) {
         throw new RuntimeException("Numerator can't be double! " + a[0] + " in " + s);
      }
      try {
         denom = Long.parseLong(a[1]);
      } catch (NumberFormatException ex) {
         throw new RuntimeException("Denominator can't be double! " + a[1] + " in " + s);
      }
      return new Lfraction(num, denom);
   }

   /** Check if string is numeric
    *
    * @param strNum
    * @return boolean
    */
   private static boolean isNumeric(String strNum) {
      try {
         double d = Double.parseDouble(strNum);
      } catch (NumberFormatException | NullPointerException nfe) {
         return false;
      }
      return true;
   }

   /**
    * Find greatest common divisor
    *
    * @param a numerator
    * @param b denominator
    * @return long divisor
    */
   private static long greatestCommonDivisor(long a, long b) {
      if (b == 0)
         return a;
      return greatestCommonDivisor(b, a % b);
   }
}
